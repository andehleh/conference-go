from django.http import JsonResponse
from .models import Attendee
from .encoders import AttendeeDetailEncoder, AttendeeListEncoder
import json
from django.views.decorators.http import require_http_methods
from events.models import Conference



@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.all()
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder = AttendeeDetailEncoder,
            safe = False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=content["conference"])
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status = 400,
            )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee, 
            encoder = AttendeeDetailEncoder,
            safe = False,
        )
    else:
        count, _= Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
