import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    print(type(state))
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}



def get_weather_data(city, state):
    
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY
    }

    url = "https://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params = params)
    
    content = response.json()
    

    longitude = content[0]["lon"]
    latitude = content[0]["lat"]
    

    params = {"lat": latitude, "lon": longitude, "units": "imperial", "appid": OPEN_WEATHER_API_KEY}
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    
    json_resp = response.json()
    
    try:
        temperature = json_resp["main"]["temp"]
        description = json_resp["weather"][0]["description"]
        return {"temp": temperature, "description": description}
    except (KeyError, IndexError):
        return None
