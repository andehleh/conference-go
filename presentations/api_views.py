from django.http import JsonResponse
from .encoders import PresentationListEncoder, PresentationDetailEncoder
from .models import Presentation
import json
from events.models import Conference
from django.views.decorators.http import require_http_methods




@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(id=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference

        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status = 400,
            )
        
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation, 
            encoder = PresentationListEncoder,
            safe = False,
        )



@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status = 400
            )
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder = PresentationDetailEncoder,
            safe = False,
        )
    else:
        count, _= Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})